// * [SECTION] Packages and Dependencies
	const express = require("express");
	const mongoose = require("mongoose");
	const dotenv = require("dotenv");
	const courseRoutes = require('./routes/courses');
	//acquire the routing components for the users collection
	const userRoutes = require('./routes/users');

// * [SECTION] Server Setup
	const app = express();
	dotenv.config(); 
	app.use(express.json());
	const secret = process.env.CONNECTION_STRING
	const port = process.env.PORT;

//[SECTION] Application Routes
	app.use('/courses', courseRoutes);
	app.use('/users', userRoutes);

// * [SECTION] Database Connet
	mongoose.connect(secret);
	let connectStatus = mongoose.connection;
	connectStatus.once('open', () => console.log('Nakakonekta ang Databse'));

// * [SECTION] Gateway Response
	app.get('/', (req, res) => {
		res.send('Ito ay para sa mga iniwan, nasaktan, at pinaasa, Welcome sa aking Pahina! Halikana sa CAMP SAWI Resort!');
	});
	app.listen(port, () => console.log(`server is running on port ${port}`));
