//[SECTION] Dependencies and Modules
	const User = require('../models/User');
	const bcrypt = require("bcrypt");
	const dotenv = require("dotenv");

//[SECTION] Environment Setup
	dotenv.config();
	const salt = parseInt(process.env.SALT);

//[SECTION] Functionalities [CREATE]
	module.exports.registerUser = (data) => {
		let fName = data.firstName;
		let lName = data.lastName;
		let email = data.email;
		let passW = data.password;
		let gendr = data.gender;
		let mobil = data.mobileNo;
		let newUser = new User({
			firstName: fName,
			lastName: lName,
			email: email,
			password: bcrypt.hashSync(passW, salt),
			gender: gendr,
			mobileNo: mobil
		});
		return newUser.save().then((savedUser, error) => {
	   	 	if (error) {
	   	 		return 'Failed to Save New User';
	   	 	} else {
	   	 		return savedUser; 
	   	 	}
   	 	});
	};
//[SECTION] Functionalities [RETRIEVE]
//[SECTION] Functionalities [UPDATE]
//[SECTION] Functionalities [DELETE]